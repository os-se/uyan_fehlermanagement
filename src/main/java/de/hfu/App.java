package de.hfu;
import java.util.Scanner;

/**
 * Hauptklasse des Praktikumprojekts
 */
public class App {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Geben Sie eine Zeichenkette ein: ");
        String input = scanner.nextLine();

        String uppercaseString = input.toUpperCase();

        System.out.println("Umgewandelte Zeichenkette in Großbuchstaben: " + uppercaseString);

        scanner.close();
    }
}

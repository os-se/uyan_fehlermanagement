package de.hfu;

import de.hfu.integration.repository.ResidentRepository;
import de.hfu.integration.domain.Resident;

import static org.junit.jupiter.api.Assertions.*;

import de.hfu.integration.service.BaseResidentService;
import de.hfu.integration.service.ResidentServiceException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class BaseResidentServiceIntegrationTest {

    private BaseResidentService residentService;
    private ResidentRepository residentRepositoryStub;

    @BeforeEach
    public void setUp() {
        // Erstellen des Stub-Objekts für das ResidentRepository
        residentRepositoryStub = new ResidentRepositoryStub(Arrays.asList(
                new Resident("Max", "Mustermann", "Musterstrasse", "Musterstadt", new Date()),
                new Resident("John", "Doe", "Main St", "City", new Date())
        ));

        // Initialisieren von BaseResidentService mit dem Stub-Objekt
        residentService = new BaseResidentService();
        residentService.setResidentRepository(residentRepositoryStub);
    }

    // Testfälle für getFilteredResidentsList()

    @Test
    public void testGetFilteredResidentsList_NoWildcard() {
        // Erstellen von Testdaten
        Resident filterResident = new Resident("Max", "Mustermann", "Musterstrasse", "Musterstadt", new Date());

        // Ausführen der Methode, die getestet werden soll
        List<Resident> result = residentService.getFilteredResidentsList(filterResident);

        // Überprüfen vom Ergebnis
        assertEquals(1, result.size());
        assertEquals("Max", result.get(0).getGivenName());
    }

    @Test
    public void testGetFilteredResidentsList_WithWildcard() {
        // Erstellen von Testdaten mit Wildcard
        Resident filterResident = new Resident("J*", "", "", "", null);

        // Ausführen der Methode, die getestet werden soll
        List<Resident> result = residentService.getFilteredResidentsList(filterResident);

        // Überprüfen vom Ergebnis
        assertEquals(1, result.size());
        assertEquals("John", result.get(0).getGivenName());
    }

    @Test
    public void testGetFilteredResidentsList_MultipleResults() {
        // Erstellen von Testdaten mit breiterem Filter
        Resident filterResident = new Resident("", "", "", "", null);

        // Ausführen der Methode, die getestet werden soll
        List<Resident> result = residentService.getFilteredResidentsList(filterResident);

        // Überprüfen vom Ergebnis
        assertEquals(2, result.size());
    }

    // Testfälle für getUniqueResident()

    @Test
    public void testGetUniqueResident_UniqueResult() {
        // Erstellen von Testdaten
        Resident filterResident = new Resident("Max", "Mustermann", "Musterstrasse", "Musterstadt", new Date());

        try {
            // Ausführen der Methode, die getestet werden soll
            Resident result = residentService.getUniqueResident(filterResident);

            // Überprüfen des Ergebnisses
            assertEquals("Max", result.getGivenName());
        } catch (ResidentServiceException e) {
            fail("Unerwartete Ausnahme: " + e.getMessage());
        }
    }

    @Test
    public void testGetUniqueResident_MultipleResults() {
        // Erstellen von Testdaten mit breiterem Filter, der zu mehreren Treffern führt
        Resident filterResident = new Resident("", "", "", "", null);

        try {
            // Ausführen der Methode, die getestet werden soll
            Resident result = residentService.getUniqueResident(filterResident);

            // Wenn der Test diesen Punkt erreicht, schlägt er fehl, weil es mehrere übereinstimmende Bewohner geben sollte
            fail("Erwartete ResidentServiceException, aber Ergebnis erhalten: " + result);
        } catch (ResidentServiceException e) {
            // Der Test ist erfolgreich, wenn eine ResidentServiceException ausgelöst wird
            assertEquals("Suchanfrage lieferte kein eindeutiges Ergebnis!", e.getMessage());
        }
    }

    @Test
    public void testGetUniqueResident_NoResult() {
        // Erstellen von Testdaten ohne übereinstimmenden Bewohner
        Resident filterResident = new Resident("NonExistent", "", "", "", null);

        try {
            // Ausführen der Methode, die getestet werden soll
            Resident result = residentService.getUniqueResident(filterResident);

            // Wenn der Test diesen Punkt erreicht, schlägt er fehl, weil es keinen übereinstimmenden Bewohner geben sollte
            fail("Erwartete ResidentServiceException, aber Ergebnis erhalten: " + result);
        } catch (ResidentServiceException e) {
            // Der Test ist erfolgreich, wenn eine ResidentServiceException ausgelöst wird
            assertEquals("Suchanfrage lieferte kein eindeutiges Ergebnis!", e.getMessage());
        }
    }
}
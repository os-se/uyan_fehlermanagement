package de.hfu;

import static org.easymock.EasyMock.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;
import de.hfu.integration.service.BaseResidentService;
import de.hfu.integration.service.ResidentServiceException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class IntegrationTestWithMock {

    private BaseResidentService residentService;
    private ResidentRepository residentRepositoryMock;

    @BeforeEach
    public void setUp() {
        // Erstellen des Mock-Objekts für das ResidentRepository
        residentRepositoryMock = createMock(ResidentRepository.class);

        // Initialisieren von BaseResidentService mit dem Mock-Objekt
        residentService = new BaseResidentService();
        residentService.setResidentRepository(residentRepositoryMock);
    }

    @AfterEach
    void tearDown() {
        verify(residentRepositoryMock);
    }

    @Test
    void testGetFilteredResidentsList() {
        // Erstellen von Testdaten
        Resident filterResident = new Resident("Max", "", "", "", null);
        List<Resident> expectedResult = Arrays.asList(new Resident("Max", "Mustermann", "Musterstrasse", "Musterstadt", new Date()));

        // Konfigurieren des Mocks
        expect(residentRepositoryMock.getResidents()).andReturn(expectedResult);
        replay(residentRepositoryMock);

        // Ausführen der Methode, die getestet werden soll
        List<Resident> result = residentService.getFilteredResidentsList(filterResident);

        // Überprüfen vom Ergebnis
        assertThat(result, is(equalTo(expectedResult)));
    }

    @Test
    void testGetUniqueResident() throws ResidentServiceException {
        // Erstellen von Testdaten
        Resident filterResident = new Resident("Max", "", "", "", null);
        Resident expectedResult = new Resident("Max", "Mustermann", "Musterstrasse", "Musterstadt", new Date());

        // Konfigurieren des Mocks
        expect(residentRepositoryMock.getResidents()).andReturn(Arrays.asList(expectedResult));
        replay(residentRepositoryMock);

        // Ausführen der Methode, die getestet werden soll
        Resident result = residentService.getUniqueResident(filterResident);

        // Überprüfen des Ergebnisses
        assertThat(result, is(equalTo(expectedResult)));
    }
}

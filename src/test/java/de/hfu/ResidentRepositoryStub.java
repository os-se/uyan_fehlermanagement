package de.hfu;

import de.hfu.integration.domain.Resident;
import de.hfu.integration.repository.ResidentRepository;

import java.util.List;

public class ResidentRepositoryStub implements ResidentRepository {
    private List<Resident> residents;

    // Konstruktor für die Testdaten
    public ResidentRepositoryStub(List<Resident> residents) {
        this.residents = residents;
    }

    @Override
    public List<Resident> getResidents() {
        return residents;
    }

    // Weitere Methoden implementieren, wenn nötig
}

package de.hfu;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import de.hfu.unit.Util;

public class HalbjahrTest {

    @Test
    public void testIstErstesHalbjahrValid() {
        assertTrue(Util.istErstesHalbjahr(1));
        assertFalse(Util.istErstesHalbjahr(12));
    }

    /*
    @Test
    public void testIstErstesHalbjahrInvalid() {
        assertFalse(Util.istErstesHalbjahr(0));
        assertFalse(Util.istErstesHalbjahr(13));
    }
    */

    @Test
    public void testIstErstesHalbjahrEdge() {
        assertFalse(Util.istErstesHalbjahr(7));
        assertTrue(Util.istErstesHalbjahr(6));
    }

    @Test
    public void testIstErstesHalbjahrIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> Util.istErstesHalbjahr(13));
    }
}
